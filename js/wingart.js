//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/
		

		include("js/initialize_plugins.js");

		function include(url){ 

				document.write('<script src="'+ url + '"></script>'); 

		}


//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/

$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
			      if ($(event.target).closest("nav,.resp_btn").length) return;
			      $("body").removeClass("show_menu");
			      if(windowW <= 991){
			      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			      }
			      event.stopPropagation();
			    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });



					// $('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link').on('click ontouchstart',function(event){
			  //       	if($("html").hasClass("md_no-touch"))return;  

			  //           var windowWidth = $(window).width(),
			  //               $parent = $(this).parent('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it');
			  //           if(windowWidth > 767){
			  //             // if($("html").hasClass("md_touch")){
			  //               if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //                 event.preventDefault();

			  //                 $parent.toggleClass('active')
			  //                  .siblings()
			  //                  .find('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link')
			  //                  .removeClass('active');
			  //               }
			  //             // }  
			  //           }
			            
			  //           else{
			                
			  //             if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //               event.preventDefault();

			  //               $parent.toggleClass('active')
			  //                .siblings()
			  //                .removeClass('active');
			  //               $parent.find("[class*='subcategories_show']")
			  //                .slideToggle()
			  //                .parents('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it')
			  //                .siblings()
			  //                .find("[class*='subcategories_show']")
			  //                .slideUp();
			  //             }
			  //           }

			  //       });


			  //       $(document).on("click ontouchstart", function(event) {
			  //         if ($(event.target).closest(".categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link").length) return;
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").removeClass("active");
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").find("[class*='subcategories_show']").css("display","none");
			  //         event.stopPropagation();
			  //       });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */




		/* ------------------------------------------------
		ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН START
		------------------------------------------------ */
				//скрипт  считывает сколько ширина скрола начало

				var scrollWidth;
				function detectScrollBarWidth(){
					var div = document.createElement('div');
						cssObj   = {"position": "absolute","top": "-9999px","width": "50px","height": "50px","overflow": "scroll"};	
					div.className = "detect_scroll_width";
					$(div).css(cssObj);
					document.body.appendChild(div);
					scrollWidth = div.offsetWidth - div.clientWidth;
					document.body.removeChild(div);
					// console.log(scrollWidth);
				}
				detectScrollBarWidth();

				// //скрипт  считывает сколько ширина скрола конец

				var div      = document.createElement('div');
					cssObj   = {"position": "fixed","bottom": "50px","right": "50px","font-size": "20px","color": "black","font-weight": "700"};
				div.className = "windowWidth";
				$(div).css(cssObj);
				document.body.appendChild(div)


				function windowWidth(target){
					var widthDiv = $(window).width();
					$("body").find(".windowWidth").text(widthDiv + scrollWidth);
					// console.log(widthDiv);
				}
				windowWidth();

				$(window).on('resize',function(){
					windowWidth();
				});

        /* ------------------------------------------------
		ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН END
		------------------------------------------------ */



		
});